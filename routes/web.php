<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('mapview');
//});

//map view
Route::get('/', 'PageController@mapView');

Route::get('/shop/{id}', 'PageController@mapView');

//shop list view
Route::get('/shops', 'PageController@shopList');
