<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//shops api
Route::get('/api_shops/{latitude}/{longitude}', 'ShopsController@getShops')->name('api_shops');

//mapview api
Route::get('/map_data/{latitude}/{longitude}', 'ShopsController@getMapData')->name('map_data');

//mapview api
Route::get('/map_data/{id}', 'ShopsController@getMapDataByShop')->name('shop_map');


