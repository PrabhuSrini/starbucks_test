<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //

  public function mapView(){
    return view('mapview');
  }

  public function shopList(){
    return view('shops');
  }

}
