<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use DB;

class ShopsController extends Controller {


  public function getShops($latitude, $longitude) {

    $circle_radius = 3000;

    $query = "id, name, street, city, latitude, longitude, ($circle_radius * acos (
        cos ( radians($latitude) )
        * cos( radians( latitude ) )
        * cos( radians( longitude ) - radians($longitude) )
        + sin ( radians($latitude) )
        * sin( radians( latitude ) )
      )
    ) AS distance";

    $shops = DB::table('shops')
      ->selectRaw($query)
      ->orderBy('distance', 'asc')
      ->paginate(8);

    return $shops->toJson();
  }

  public function getMapData($latitude,$longitude) {
    $locations = ShopsController::getLocationData($latitude, $longitude);
    return $locations->toJson();
  }



  public function getMapDataByShop($id) {

    $shop_location = DB::table('shops')
      ->where('id', $id)
      ->first();
    if ($shop_location->latitude && $shop_location->longitude) {
      $latitude = $shop_location->latitude;
      $longitude = $shop_location->longitude;

      $locations = ShopsController::getLocationData($latitude, $longitude);
      $locations[] = ['latitude' => $latitude, 'longitude' => $longitude];
      return $locations->toJson();
    }
  }


  public function getLocationData($latitude, $longitude) {

    $circle_radius = 3000;

    $query = "name, latitude, longitude, ($circle_radius * acos (
        cos ( radians($latitude) )
        * cos( radians( latitude ) )
        * cos( radians( longitude ) - radians($longitude) )
        + sin ( radians($latitude) )
        * sin( radians( latitude ) )
      )
    ) AS distance";

    $locations = DB::table('shops')
      ->selectRaw($query)
      ->orderBy('distance', 'asc')
      ->limit(50)
      ->get();

    return $locations;

  }

}
