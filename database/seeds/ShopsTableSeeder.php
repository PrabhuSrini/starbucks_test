<?php

use Illuminate\Database\Seeder;
use App\Shop;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      Shop::truncate();

      $json = File::get("database/data/starbucks_new_york.json");
      $data = json_decode($json);
      foreach ($data as $item) {
        Shop::create([
          'id' => $item->id,
          'name' => $item->name,
          'street' => $item->street,
          'city' => $item->city,
          'latitude' => $item->location->latitude,
          'longitude' => $item->location->longitude,
        ]);
      }
    }
}
