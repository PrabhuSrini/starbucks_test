
@extends('layouts.app')

@section('content')
    <div id="app" class="row">
         <div class="map-container">
                <map-view id="map-view"></map-view>
         </div>
    </div>
@endsection