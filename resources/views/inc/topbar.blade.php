
<div class="top-bar row">
    <div class="col-md-8 col-sm-8 col-xs-8 title"><a href="/">Starbucks</a></div>
    @if(Request::is('shops'))
        <div class="col-md-4 col-sm-4 col-xs-4 nav-link"><a href="/" class="pull-right"> Around Me</a></div>
    @else
        <div class="col-md-4 col-sm-4 col-xs-4 nav-link"><a href="/shops" class="pull-right"> Shops</a></div>
     @endif

</div>