<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}'}</script>
    <link href="/css/app.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3 main-container">

            @include('inc.topbar')
            @yield('content')

        </div>
    </div>
</div>

<footer class="text-center">
    <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3 sb-footer">
    <p>Copyright 2018 &copy; Starbucks test</p>
    </div>
    </div>
    </div>
</footer>


<script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyD8ZhR7EwrGS29MiuxJcL7X_mPcaASoUYI"></script>
<script src="{{ asset('js/app.js') }}"></script>


</body>
</html>